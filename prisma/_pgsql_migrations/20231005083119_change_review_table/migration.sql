/*
  Warnings:

  - You are about to drop the column `comment` on the `review` table. All the data in the column will be lost.
  - Added the required column `content` to the `review` table without a default value. This is not possible if the table is not empty.
  - Added the required column `title` to the `review` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "review" DROP COLUMN "comment",
ADD COLUMN     "content" TEXT NOT NULL,
ADD COLUMN     "title" TEXT NOT NULL;
