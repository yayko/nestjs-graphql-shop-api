import { PrismaClient } from '@prisma/client';
import { faker } from '@faker-js/faker';
import { readdirSync } from 'fs';
import { join } from 'path';

const prisma = new PrismaClient();

const categories = [];

const collectionsCount = 2;
const collectionIds: Array<string> = [];

const seedCategories = async () => {
  categories['t-shirts'] = await prisma.category.create({
    data: {
      name: 'T-shirts',
      slug: 't-shirts',
      description: faker.lorem.paragraph(2),
    },
  });
  categories['hoodies'] = await prisma.category.create({
    data: {
      name: 'Hoodies',
      slug: 'hoodies',
      description: faker.lorem.paragraph(2),
    },
  });
  categories['shoes'] = await prisma.category.create({
    data: {
      name: 'Shoes',
      slug: 'shoes',
      description: faker.lorem.paragraph(2),
    },
  });
};

const seedProducts = async () => {
  const images = readdirSync(join(__dirname, '../public'));

  for (let i = 0; i < images.length - 1; i++) {
    const name = getProductNameFromImageFileName(images[i]);
    const category = getCategory(images[i]);

    if (!category?.id) continue;

    await prisma.product.create({
      data: {
        name,
        slug: faker.helpers.slugify(name).toLocaleLowerCase(),
        description: faker.lorem.paragraph(10),
        price: (Math.floor(Math.random() * 250) + 70) * 100,
        createdAt: faker.date.past(),
        categoryId: getCategory(images[i]).id,
        image: images[i],
      },
    });
  }
};

const seedCollections = async () => {
  const products = await prisma.product.findMany();

  for (let i = 0; i < collectionsCount; i++) {
    const name = faker.commerce.department() + ' collection';

    if (await prisma.collection.findFirst({ where: { name } })) {
      i--;
      continue;
    }

    const collection = await prisma.collection.create({
      data: {
        name,
        slug: faker.helpers.slugify(name).toLocaleLowerCase(),
        description: faker.lorem.paragraph(1),
      },
    });
    collectionIds.push(collection.id);

    // link n random products to collection
    const elementsCount = Math.floor(Math.random() * 10) + 4;
    const randomProducts = getRandomArrayElements(products, elementsCount);
    for (const product of randomProducts) {
      await prisma.collectionProduct.create({
        data: {
          collectionId: collection.id,
          productId: product.id,
        },
      });
    }
  }
};

const seedRatngs = async () => {
  const products = await prisma.product.findMany({
    select: {
      id: true,
      name: true,
    },
  });

  const ratings = [3, 4, 5];

  products.forEach(async ({ id, name }) => {
    const raitingsCount = Math.floor(Math.random() * 15) + 10;
    console.log(
      'Product: ',
      name,
      ', generating[' + raitingsCount + '] ratings',
    );
    const reviews = [];
    for (let i = 0; i < raitingsCount; i++) {
      const randomRating = ratings[Math.floor(Math.random() * ratings.length)];

      reviews.push({
        productId: id,
        rating: randomRating,
        title: faker.lorem.paragraph(1),
        content: faker.lorem.paragraph(Math.floor(Math.random() * 12) + 8),
        email: faker.internet.email(),
        name: faker.person.firstName(),
      });
    }
    const avgRating =
      reviews.reduce((acc, curr) => acc + curr.rating, 0) / reviews.length;

    await prisma.review.createMany({
      data: reviews,
    });

    await prisma.product.update({
      where: { id },
      data: {
        avgRating: Math.round(avgRating * 10) / 10,
      },
    });
  });
};

const getRandomArrayElements = (arr: Array<any>, count: number = 1) => {
  const shuffled = arr.sort(() => 0.5 - Math.random());
  return shuffled.slice(0, count);
};

const getProductNameFromImageFileName = (imageFileName: string) => {
  return imageFileName
    .split('.')[0]
    .split('-')
    .map((word) =>
      word === 'tshirt'
        ? 'T-shirt'
        : word.charAt(0).toUpperCase() + word.slice(1),
    )
    .join(' ');
};

const getCategory = (imageName: string) => {
  console.log(imageName);
  if (imageName.includes('-shoes')) {
    return categories['shoes'];
  } else if (imageName.includes('-hoodie')) {
    return categories['hoodies'];
  } else if (imageName.includes('-tshirt')) {
    return categories['t-shirts'];
  }
};

(async function () {
  await seedCategories();
  await seedProducts();
  await seedCollections();
  await seedRatngs();
})();
