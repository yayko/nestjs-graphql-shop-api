update-repo:
	git pull
	pnpm install
	pnpm prisma generate
	pnpm prisma migrate deploy
	pnpm build
	touch tmp/restart.txt
	curl https://pystore.pl -I