import { Review } from '@/common/models/review.model';
import { ReviewAddDto } from '@/modules/review/dto/review-add.dto';
import { ReviewService } from '@/modules/review/review.service';
import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';

@Resolver(() => Review)
export class ReviewResolver {
  constructor(private reviewService: ReviewService) {}

  @Query(() => [Review!])
  async reviews(@Args('productId') productId: string) {
    return this.reviewService.getProductReviews(productId);
  }

  @Mutation(() => Review!)
  async reviewCreate(@Args('data') data: ReviewAddDto) {
    return this.reviewService.createReview(data);
  }
}
