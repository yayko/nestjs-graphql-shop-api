import { PrismaService } from '@/common/prisma/prisma.service';
import { ReviewAddDto } from '@/modules/review/dto/review-add.dto';
import { Injectable } from '@nestjs/common';

@Injectable()
export class ReviewService {
  constructor(private prisma: PrismaService) {}

  async getProductReviews(productId: string) {
    return await this.prisma.review.findMany({
      where: {
        productId,
      },
      orderBy: {
        createdAt: 'desc',
      },
    });
  }
  async createReview(data: ReviewAddDto) {
    const review = await this.prisma.review.create({
      data,
    });
    const reviewAggregate = await this.prisma.review.aggregate({
      where: {
        productId: data.productId,
      },
      _avg: {
        rating: true,
      },
    });

    // Update avg rating of product
    console.log(Math.round(reviewAggregate._avg.rating * 10) / 10);
    await this.prisma.product.update({
      where: { id: data.productId },
      data: {
        avgRating: Math.round(reviewAggregate._avg.rating * 10) / 10,
      },
    });

    return review;
  }
}
