import { Field, InputType, Int, ObjectType } from '@nestjs/graphql';

@InputType()
@ObjectType()
export class ReviewAddDto {
  @Field(() => Int!)
  rating: number;

  @Field(() => String!)
  title: string;

  @Field(() => String!)
  content: string;

  @Field(() => String!)
  name: string;

  @Field(() => String!)
  email: string;

  @Field(() => String!)
  productId: string;
}
