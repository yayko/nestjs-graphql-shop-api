import { PrismaService } from '@/common/prisma/prisma.service';
import { Injectable } from '@nestjs/common';

@Injectable()
export class CategoryService {
  constructor(private prisma: PrismaService) {}

  async findOneById(id: string) {
    const category = this.prisma.category.findUnique({
      where: {
        id,
      },
    });

    return category;
  }

  async findOneBySlug(slug: string) {
    const category = this.prisma.category.findUnique({
      where: {
        slug,
      },
    });

    return category;
  }

  async findAll(take?: number, skip?: number) {
    return this.prisma.category.findMany({
      take,
      skip: skip || 0,
    });
  }
}
