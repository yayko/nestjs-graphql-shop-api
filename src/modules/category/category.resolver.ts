import { PaginationArgs } from '@/common/decorators/args/pagination.args';
import { ProductsListArgs } from '@/common/decorators/args/products-list.args';
import { Category } from '@/common/models/category.model';
import { Product } from '@/common/models/product.model';
import { CategoryService } from '@/modules/category/category.service';
import { ProductService } from '@/modules/product/product.service';
import { Args, Parent, Query, ResolveField, Resolver } from '@nestjs/graphql';

@Resolver(() => Category)
export class CategoryResolver {
  constructor(
    private categoryService: CategoryService,
    private productService: ProductService,
  ) {}

  @Query(() => Category)
  async category(@Args('slug') slug: string) {
    return this.categoryService.findOneBySlug(slug);
  }

  @Query(() => [Category])
  async categories(@Args() { skip, take }: PaginationArgs) {
    return this.categoryService.findAll(skip, take);
  }

  @ResolveField('products', () => [Product])
  async products(
    @Args() { where, skip, sort, take }: ProductsListArgs,
    @Parent() category: Category,
  ) {
    return this.productService.findAll(
      {
        category_slug: category.slug,
        ...where,
      },
      take,
      skip,
      sort,
    );
  }
}
