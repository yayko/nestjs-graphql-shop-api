import { PrismaService } from '@/common/prisma/prisma.service';
import { CategoryResolver } from '@/modules/category/category.resolver';
import { CategoryService } from '@/modules/category/category.service';
import { ProductService } from '@/modules/product/product.service';
import { Module } from '@nestjs/common';

@Module({
  providers: [CategoryResolver, CategoryService, PrismaService, ProductService],
})
export class CategoryModule {}
