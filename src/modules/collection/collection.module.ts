import { PrismaService } from '@/common/prisma/prisma.service';
import { CollectionResolver } from '@/modules/collection/collection.resolver';
import { CollectionService } from '@/modules/collection/collection.service';
import { ProductService } from '@/modules/product/product.service';
import { Module } from '@nestjs/common';

@Module({
  providers: [
    CollectionResolver,
    CollectionService,
    PrismaService,
    ProductService,
  ],
})
export class CollectionModule {}
