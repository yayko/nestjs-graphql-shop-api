import { PrismaService } from '@/common/prisma/prisma.service';
import { Injectable } from '@nestjs/common';

@Injectable()
export class CollectionService {
  constructor(private prisma: PrismaService) {}

  async findOneById(id: string) {
    return this.prisma.collection.findUnique({
      where: {
        id,
      },
    });
  }
  async findOneBySlug(slug: string) {
    return this.prisma.collection.findUnique({
      where: {
        slug,
      },
    });
  }

  async findAll(take?: number, skip?: number) {
    return this.prisma.collection.findMany({
      take,
      skip: skip || 0,
    });
  }
}
