import { PaginationArgs } from '@/common/decorators/args/pagination.args';
import { ProductsListArgs } from '@/common/decorators/args/products-list.args';
import { Collection } from '@/common/models/collection.model';
import { CollectionService } from '@/modules/collection/collection.service';
import { ProductService } from '@/modules/product/product.service';
import { Args, Parent, Query, ResolveField, Resolver } from '@nestjs/graphql';

@Resolver(() => Collection)
export class CollectionResolver {
  constructor(
    private collectionService: CollectionService,
    private productService: ProductService,
  ) {}

  @Query(() => Collection)
  async collection(@Args('slug') slug: string) {
    return this.collectionService.findOneBySlug(slug);
  }

  @Query(() => [Collection])
  async collections(@Args() { skip, take }: PaginationArgs) {
    return this.collectionService.findAll(skip, take);
  }

  @ResolveField('products', () => [Collection])
  async products(
    @Args() { where, skip, sort, take }: ProductsListArgs,
    @Parent() collection: Collection,
  ) {
    return this.productService.findAll(
      {
        collection_slug: collection.slug,
        ...where,
      },
      take,
      skip,
      sort,
    );
  }
}
