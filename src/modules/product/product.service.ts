import { PrismaService } from '@/common/prisma/prisma.service';
import { ProductSortType } from '@/types';
import { getOrderByFromQuery } from '@/common/utils/queryParamsUtils';
import { Injectable, NotFoundException } from '@nestjs/common';
import { Prisma } from '@prisma/client';
import { ProductsWhereInput } from '@/common/decorators/args/product.products-where.input';

@Injectable()
export class ProductService {
  constructor(private prisma: PrismaService) {}

  async findOneBy(by: 'id' | 'slug', value: string) {
    const product = await this.prisma.product.findUnique({
      where: by === 'id' ? { id: value } : { slug: value },
      include: {
        category: true,
        collections: {
          include: {
            collection: true,
          },
        },
      },
    });

    if (!product) throw new NotFoundException('Not found');

    return {
      ...product,
      image: `${process.env.APP_URL}/${product.image}`,
      collections: product.collections.map(
        (collection) => collection.collection,
      ),
    };
  }

  async findAll(
    whereArgs: ProductsWhereInput,
    take?: number,
    skip?: number,
    sort?: string,
  ) {
    const orderBy = getOrderByFromQuery(sort as ProductSortType);
    const where = this.getWhereArgs(whereArgs);

    const queryResult = await this.prisma.product.findMany({
      where,
      take,
      skip,
      orderBy,
      include: {
        category: true,
        collections: {
          include: {
            collection: true,
          },
        },
      },
    });

    const products = queryResult.map((product) => {
      return {
        ...product,
        image: `${process.env.APP_URL}/${product.image}`,
        collections: product.collections.map(
          (collection) => collection.collection,
        ),
      };
    });

    return products || [];
  }

  async productTotal(whereArgs: ProductsWhereInput) {
    const where = this.getWhereArgs(whereArgs);
    const total = await this.prisma.product.count({
      where,
    });

    return {
      total,
    };
  }

  getWhereArgs(whereArgs: ProductsWhereInput) {
    const {
      name_contains,
      category_slug,
      collection_slug,
      price_gte,
      price_lte,
    } = whereArgs || {};
    const collectionsFilterArgs: Prisma.CollectionProductListRelationFilter =
      collection_slug
        ? {
            some: {
              collection: {
                slug: collection_slug,
              },
            },
          }
        : undefined;

    const where: Prisma.ProductWhereInput = {
      AND: [
        {
          name: name_contains
            ? {
                contains: name_contains,
              }
            : undefined,
          price: {
            gte: price_gte || undefined,
            lte: price_lte || undefined,
          },
          category: category_slug
            ? {
                slug: category_slug,
              }
            : undefined,
          collections: collectionsFilterArgs,
        },
      ],
    };

    return where;
  }
}
