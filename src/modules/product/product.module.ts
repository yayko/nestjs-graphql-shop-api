import { PrismaService } from '@/common/prisma/prisma.service';
import { ProductResolver } from '@/modules/product/product.resolver';
import { ProductService } from '@/modules/product/product.service';
import { Module } from '@nestjs/common';

@Module({
  providers: [ProductResolver, ProductService, PrismaService],
})
export class ProductModule {}
