import { ProductService } from '@/modules/product/product.service';
import { Args, Resolver, Query, ID } from '@nestjs/graphql';

import { ProductsListArgs } from '@/common/decorators/args/products-list.args';
import { Product } from '@/common/models/product.model';
import { ProductTotal } from '@/common/models/product-total.model';
import { ProductsWhereInput } from '@/common/decorators/args/product.products-where.input';

@Resolver(() => Product)
export class ProductResolver {
  constructor(private productService: ProductService) {}

  @Query(() => Product)
  async product(@Args('id', ID) id: string) {
    return this.productService.findOneBy('id', id);
  }

  @Query(() => Product)
  async productBySlug(@Args('slug') slug: string) {
    return this.productService.findOneBy('slug', slug);
  }

  @Query(() => [Product])
  async products(@Args() { where, take, skip, sort }: ProductsListArgs) {
    return this.productService.findAll(where, take, skip, sort);
  }

  @Query(() => ProductTotal)
  async productsTotal(@Args('where') where: ProductsWhereInput) {
    return this.productService.productTotal(where);
  }
}
