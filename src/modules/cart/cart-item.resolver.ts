import { CartItem } from '@/common/models/cart-item.model';
import { Product } from '@/common/models/product.model';
import { PrismaService } from '@/common/prisma/prisma.service';
import { CartService } from '@/modules/cart/cart.service';
import { CartItemAddDto } from '@/modules/cart/dto/cart-item-add.dto';
import { CartItemRemoveDto } from '@/modules/cart/dto/cart-item-remove.dto';
import { CartItemUpdatedDto } from '@/modules/cart/dto/cart-item-update.dto';
import { ProductService } from '@/modules/product/product.service';
import {
  Args,
  Mutation,
  Parent,
  ResolveField,
  Resolver,
} from '@nestjs/graphql';

@Resolver(() => CartItem)
export class CartItemResolver {
  constructor(
    private prisma: PrismaService,
    private cartService: CartService,
    private productService: ProductService,
  ) {}

  @Mutation(() => CartItem)
  async cartItemAdd(@Args('data') { cartId, productId }: CartItemAddDto) {
    const cart = await this.cartService.getCart(cartId);

    if (!cart) {
      throw new Error('Cart not found. Cannot add item.');
    }

    const cartItem = await this.prisma.cartItem.findFirst({
      where: { productId, cartId: cart.id },
    });

    return cartItem
      ? this.cartService.incrementItem(cartItem.id)
      : this.cartService.addItem(cart.id, productId);
  }

  @Mutation(() => CartItem)
  async cartItemUpdateQuantity(
    @Args('data') { id, quantity }: CartItemUpdatedDto,
  ) {
    return this.cartService.updateQuantity(id, quantity);
  }

  @Mutation(() => CartItem)
  async cartItemRemove(@Args('data') { id }: CartItemRemoveDto) {
    return this.cartService.removeItem(id);
  }

  @ResolveField(() => Product)
  async product(@Parent() cartItem: CartItem) {
    return this.productService.findOneBy('id', cartItem.productId);
  }
}
