import { PrismaService } from '@/common/prisma/prisma.service';
import { CartItemResolver } from '@/modules/cart/cart-item.resolver';
import { CartResolver } from '@/modules/cart/cart.resolver';
import { CartService } from '@/modules/cart/cart.service';
import { ProductService } from '@/modules/product/product.service';
import { Module } from '@nestjs/common';

@Module({
  providers: [
    CartService,
    CartResolver,
    CartItemResolver,
    PrismaService,
    ProductService,
  ],
})
export class CartModule {}
