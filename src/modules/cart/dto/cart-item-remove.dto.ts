import { Field, InputType, ObjectType } from '@nestjs/graphql';

@InputType()
@ObjectType()
export class CartItemRemoveDto {
  @Field({ description: 'The ID of the CartItem entity' })
  id: string;
}
