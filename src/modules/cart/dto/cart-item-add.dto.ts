import { Field, InputType, ObjectType } from '@nestjs/graphql';

@InputType()
@ObjectType()
export class CartItemAddDto {
  @Field()
  cartId: string;

  @Field()
  productId: string;
}
