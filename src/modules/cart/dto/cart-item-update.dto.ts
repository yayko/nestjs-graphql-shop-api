import { Field, InputType, Int, ObjectType } from '@nestjs/graphql';

@InputType()
@ObjectType()
export class CartItemUpdatedDto {
  @Field({ description: 'The ID of the CartItem entity' })
  id: string;

  @Field(() => Int)
  quantity: number;
}
