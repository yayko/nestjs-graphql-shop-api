import { CartItem } from '@/common/models/cart-item.model';
import { CartTotals } from '@/common/models/cart-totals.model';
import { Cart } from '@/common/models/cart.model';
import { CartService } from '@/modules/cart/cart.service';
import {
  Args,
  ID,
  Mutation,
  Parent,
  Query,
  ResolveField,
  Resolver,
} from '@nestjs/graphql';

@Resolver(() => Cart)
export class CartResolver {
  constructor(private cartService: CartService) {}

  @Query(() => Cart, { nullable: true })
  async cart(@Args('id', ID) id: string) {
    return this.cartService.getCart(id);
  }

  @Query(() => CartTotals)
  async cartTotals(@Args('id', ID) id: string) {
    return this.cartService.getCartTotal(id);
  }

  @Mutation(() => Cart, { nullable: true })
  async cartCreate() {
    return this.cartService.createCart();
  }

  @ResolveField(() => [CartItem!])
  async cartItems(@Parent() cart: Cart) {
    return this.cartService.getCartItems(cart.id);
  }
}
