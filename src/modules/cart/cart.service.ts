import { PrismaService } from '@/common/prisma/prisma.service';
import { Injectable } from '@nestjs/common';

@Injectable()
export class CartService {
  constructor(private prisma: PrismaService) {}

  async getCartItems(cartId: string) {
    const cartItems = await this.prisma.cartItem.findMany({
      where: { cartId },
      orderBy: {
        created_at: 'desc',
      },
    });
    return cartItems;
  }

  async getCart(id: string) {
    return await this.prisma.cart.findUnique({
      where: {
        id,
      },
    });
  }

  async getCartTotal(id: string) {
    const cart = await this.getCart(id);
    if (!cart) {
      return {
        productsCount: 0,
        productsTotalPrice: 0,
      };
    }

    const cartProducts = await this.prisma.cartItem.findMany({
      where: {
        cartId: cart.id,
      },
      select: {
        product: {
          select: {
            price: true,
          },
        },
        quantity: true,
      },
    });

    const productsCount = cartProducts.length;

    const productsTotalPrice = cartProducts.reduce(
      (total, cartItem) => total + cartItem.product.price * cartItem.quantity,
      0,
    );

    return {
      productsCount,
      productsTotalPrice,
    };
  }

  async createCart() {
    return await this.prisma.cart.create({
      data: {},
    });
  }

  async addItem(cartId: string, productId: string) {
    return this.prisma.cartItem.create({
      data: {
        cartId,
        productId,
        quantity: 1,
      },
    });
  }

  async updateQuantity(cartId: string, quantity: number) {
    const cartItem = await this.prisma.cartItem.findUnique({
      where: { id: cartId },
    });

    if (!cartItem) {
      throw new Error('Cart item not found');
    }
    return this.prisma.cartItem.update({
      where: {
        id: cartId,
      },
      data: {
        quantity: quantity <= 0 ? 1 : quantity,
      },
    });
  }

  async incrementItem(id: string) {
    return this.prisma.cartItem.update({
      where: {
        id,
      },
      data: {
        quantity: {
          increment: 1,
        },
      },
    });
  }

  async decrementItem(id: string) {
    return this.prisma.cartItem.update({
      where: {
        id,
      },
      data: {
        quantity: {
          decrement: 1,
        },
      },
    });
  }

  async removeItem(id: string) {
    return this.prisma.cartItem.delete({
      where: {
        id,
      },
    });
  }
}
