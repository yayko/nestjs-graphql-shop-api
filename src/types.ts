export type ProductSortType =
  | 'name'
  | '-name'
  | 'price'
  | '-price'
  | 'avgRating'
  | '-avgRating';
