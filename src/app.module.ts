import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo';
import { join } from 'path';
import { ApolloServerPluginLandingPageLocalDefault } from '@apollo/server/plugin/landingPage/default';
import { ProductModule } from '@/modules/product/product.module';
import { ConfigModule } from '@nestjs/config';
import { CollectionModule } from '@/modules/collection/collection.module';
import { CategoryModule } from '@/modules/category/category.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import { CartModule } from '@/modules/cart/cart.module';
import { ReviewModule } from '@/modules/review/review.module';

@Module({
  imports: [
    ProductModule,
    CollectionModule,
    CategoryModule,
    CartModule,
    ReviewModule,
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    GraphQLModule.forRoot<ApolloDriverConfig>({
      driver: ApolloDriver,
      playground: false,
      introspection: true,
      plugins: [ApolloServerPluginLandingPageLocalDefault()],
      autoSchemaFile: join(process.cwd(), 'src/schema.gql'),
      buildSchemaOptions: {
        numberScalarMode: 'integer',
      },
      context: ({ req, res }) => ({ req, res }),
    }),
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', '../public'), // added ../ to get one folder back
      serveRoot: '/public/', //last slash was important
    }),
  ],
})
export class AppModule {}
