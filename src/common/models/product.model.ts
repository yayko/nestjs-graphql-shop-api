import { Category } from '@/common/models/category.model';
import { Collection } from '@/common/models/collection.model';
import { Field, Float, Int, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class Product {
  @Field()
  id: string;

  @Field()
  name: string;

  @Field()
  slug: string;

  @Field()
  description: string;

  @Field(() => Int)
  price: number;

  @Field()
  image: string;

  @Field(() => String)
  category_id: string;

  @Field(() => Float)
  avgRating: number;

  @Field(() => Category)
  category: Category;

  @Field(() => [Collection!])
  collections: Collection[];
}
