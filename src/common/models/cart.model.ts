import { CartItem } from '@/common/models/cart-item.model';
import { Field, ID, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class Cart {
  @Field(() => ID)
  id: string;

  @Field(() => [CartItem!])
  cartItems: CartItem[];
}
