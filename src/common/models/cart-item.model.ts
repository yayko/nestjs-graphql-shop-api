import { Cart } from '@/common/models/cart.model';
import { Product } from '@/common/models/product.model';
import { Field, ID, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class CartItem {
  @Field(() => ID)
  id: string;

  @Field(() => String)
  cartId: string;

  @Field(() => Number)
  quantity: number;

  @Field(() => String)
  productId: string;

  @Field(() => String)
  productVariantId: string;

  @Field(() => Date)
  createdAt: Date;

  @Field(() => Cart)
  cart: Cart;

  @Field(() => Product)
  product: Product;
}
