import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class BaseSlugModel {
  @Field()
  id: string;

  @Field()
  slug: string;

  @Field()
  name: string;
}
