import { Product } from '@/common/models/product.model';
import { Field, Int, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class Review {
  @Field()
  id: string;

  @Field()
  name: string;

  @Field()
  email: string;

  @Field(() => Int)
  rating: number;

  @Field()
  title: string;

  @Field()
  content: string;

  @Field()
  productId: string;

  @Field()
  createdAt: string;

  @Field(() => Product)
  product: Product;
}
