import { BaseSlugModel } from '@/common/models/base-slug-model';
import { Product } from '@/common/models/product.model';
import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class Category extends BaseSlugModel {
  @Field(() => [Product])
  products: Product[];
}
