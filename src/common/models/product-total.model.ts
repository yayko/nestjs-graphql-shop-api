import { Field, Int, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class ProductTotal {
  @Field(() => Int)
  total: number;
}
