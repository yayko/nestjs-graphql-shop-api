import { Field, Int, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class CartTotals {
  @Field(() => Int)
  productsCount: number;

  @Field(() => Int)
  productsTotalPrice: number;
}
