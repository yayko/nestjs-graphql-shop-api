const AvailableProductSortValues = [
  'name',
  '-name',
  'price',
  '-price',
  'avgRating',
  '-avgRating',
];
const DefaultPaginationPageSize = 4;

export { AvailableProductSortValues, DefaultPaginationPageSize };
