import { AvailableProductSortValues } from '@/common/constants';
import { ProductSortType } from '@/types';

export const getOrderByFromQuery = (
  sort?: ProductSortType,
): Record<string, 'asc' | 'desc'> | undefined =>
  !sort || !AvailableProductSortValues.includes(sort)
    ? undefined
    : { [sort.replace('-', '')]: sort.startsWith('-') ? 'desc' : 'asc' };
