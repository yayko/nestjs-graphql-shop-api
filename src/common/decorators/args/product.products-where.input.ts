import { PaginationArgs } from '@/common/decorators/args/pagination.args';
import { Field, InputType, Int, ObjectType } from '@nestjs/graphql';

@InputType()
@ObjectType()
export class ProductsWhereInput extends PaginationArgs {
  @Field({
    nullable: true,
  })
  name_contains?: string;

  @Field({
    nullable: true,
  })
  category_name_contains?: string;

  @Field({
    nullable: true,
  })
  category_slug?: string;

  @Field({
    nullable: true,
  })
  collection_slug?: string;

  @Field(() => Int, {
    nullable: true,
  })
  price_gte?: number;

  @Field(() => Int, {
    nullable: true,
  })
  price_lte?: number;
}
