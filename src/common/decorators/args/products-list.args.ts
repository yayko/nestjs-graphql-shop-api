import { PaginationArgs } from '@/common/decorators/args/pagination.args';
import { ProductsWhereInput } from '@/common/decorators/args/product.products-where.input';
import { ProductSortType } from '@/types';
import { ArgsType, Field } from '@nestjs/graphql';

@ArgsType()
export class ProductsListArgs extends PaginationArgs {
  @Field(() => ProductsWhereInput, { nullable: true })
  where?: ProductsWhereInput;

  @Field({ nullable: true })
  sort?: ProductSortType;

  @Field({ nullable: true })
  take?: number;

  @Field({ nullable: true })
  skip?: number;
}
