import { serialize } from 'cookie';
import { Response } from 'express';

export default class Cookies {
  static setCookieIdHeader(res: Response, { cartId }: { cartId: string }) {
    res.setHeader('set-cookie', Cookies.getSerializedCartIdCookie({ cartId }));
  }

  static removeCookieIdHeader(res: Response) {
    res.setHeader('set-cookie', [
      serialize(`cartId`, '', {
        expires: new Date(Date.now() - 1),
        // httpOnly: true,
        sameSite: true,
        path: '/',
      }),
    ]);
  }

  static getSerializedCartIdCookie({ cartId }) {
    const maxAge = 60 * 60 * 24 * 3 * 1000;
    return serialize('cartId', cartId, {
      expires: new Date(Date.now() + maxAge),
      // httpOnly: true,
      secure: process.env.NODE_ENV === 'production',
      path: '/',
    });
  }
}
