import { Request, Response } from 'express';

declare namespace Express {}

export {};

declare global {
  namespace Express {
    export interface User {
      readonly id: number | null;
      readonly email: string | null;
      readonly first_name?: string;
    }
  }

  export interface GraphQLContext {
    req: Request;
    res: Response;
  }
}
